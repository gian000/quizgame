package io.wikiquiz.game.util;

/**
 * 
 * StringUtils class
 * All defined constants needed for the singleplayer mode and multiplayer mode
 *
 */
public final class StringUtils {
	private StringUtils() {}
	
	public static final String[] ENTITY_TYPES = {"Person", "Company", "Band"};
	public static final String[] THEMES = {"Sports", "Science", "Politics", "Music", "Economy"};
	
	// the only important thing regarding to templates is: (X stands for the entity type)
	// if you want to add a new predicate, then you have to:
	// add the predicate to X_PREDICATES[]
	// add a template to X_TEMPLATES[]
	// important: the order has to be the same for all the 2 arrays. If birthdate is first, then birthdate has to be first in all 2 arrays.
	// --- PERSON ---
	public static final String[] PERSON_PREDICATES = {"dbo:mainInterest", "dbo:birthDate", "dbo:birthPlace", "dbo:height", "dct:description", "dbo:knownFor",
			"dbo:award", "dbo:position", "dbo:team", "dbp:nationality", "dbp:koWins", "dbp:wins", "dbp:motogpChampionships", "dbp:pga", "dbo:plays",
			"dbp:doublestitles", "dbp:singlestitles", "dbp:singlesrecord", "dbp:highestsinglesranking", "dbo:battingSide", "dbo:throwingSide",
			"dbp:ridertype", "dbp:club", "dbo:championships"};
	
	// String templates for person
	public static final String[] PERSON_TEMPLATES = {"One of the person's main interests was %s", "Was born on %s", "Was born in %s", "Is %s tall", "Is %s", "Is known for %s", "Won the '%s' award",
			"Is %s", "Played for %s among others", "Is %s", "Has %s K.O. wins", "Has %s wins", "Won %s MotoGP championships", "Won %sx PGA Tour",
			"Plays %s", "Won %s doubles titles (2v2)", "Won %s singles titles (1v1)", "Won %s matches (1v1)", "Player's heighest rank was: %s (1v1)",
			"Player's batting side is %s", "Player's throwing side is %s", "Rider's type is %s", "Played for the club %s among others", "Won %s F1 championships"};
	
	// --- COMPANY ---
	public static final String[] COMPANY_PREDICATES = {"dbo:foundationPlace", "dbo:industry", "dbo:locationCity", "dbo:numberOfEmployees", "dbo:product",
			"dbo:service", "dbo:foundedBy", "dbp:owner", "dbp:founders", "dbo:distributor", "dbo:keyPerson", "dbp:numLocations"};
	
	// String templates for company
	public static final String[] COMPANY_TEMPLATES = {"Company's foundation place is %s", "Is involved in %s industry, among others", "Is located in %s", "Has about %s employees",
			"Is known for its product '%s'", "Is known for its service '%s'", "Was founded by %s", "One of the owners is %s", "One of the founders is %s",
			"Distributed %s", "One of the key persons is %s", "There are %s stores in the world"};

	// --- BAND ---
	public static final String[] BAND_PREDICATES = {"dbo:bandMember", "dbo:genre", "dbo:hometown", "dbo:recordLabel", "dbo:activeYearsStartYear", "dbo:musicalArtist", "dbo:artist"};
	
	// String templates for band
	public static final String[] BAND_TEMPLATES = {"One of the band members is %s", "Band's genre is %s", "Band's hometown is %s",
			"One of the record labels is %s", "Band started to be active in %s", "Is musical artist of %s", "One of their song is %s"};

	// Theme/Topic: Sports (Person)
		public static final String[] SPORTS_PERSON = {"Franz_Beckenbauer", "Miroslav_Klose", "Oliver_Kahn", "Xabi_Alonso", "Fernando_Torres", "Iker_Casillas", "Michael_Ballack", "Jürgen_Klinsmann",
				"David_Beckham", "Thierry_Henry", "David_Trezeguet", "Patrick_Vieira", "Zinedine_Zidane", "Alessandro_Del_Piero", "Gianluigi_Buffon", "Ronaldinho", "Zlatan_Ibrahimović", "Cristiano_Ronaldo",
				"Philipp_Lahm", "Lionel_Messi", "Manuel_Neuer", "Gareth_Bale", "Arjen_Robben", "Neymar", "Roger_Federer", "LeBron_James", "Stephen_Curry", "Tiger_Woods", "Kirk_Cousins",
				"Serena_Williams", "Kevin_Durant", "Tyson_Fury", "Russell_Westbrook", "Anthony_Joshua", "Deontay_Wilder", "Rafael_Nadal", "Paul_Pogba", "Clayton_Kershaw", "Stephen_Strasburg",
				"Mike_Trout", "Jon_Lester", "Mike_Tyson", "Muhammad_Ali", "Vitali_Klitschko", "Lennox_Lewis", "Wladimir_Klitschko", "Max_Scherzer", "Conor_McGregor", "Usain_Bolt", "Tom_Brady",
				"Kobe_Bryant", "Carl_Lewis", "Asafa_Powell", "Maria_Sharapova", "Li_Na", "Kei_Nishikori", "Blake_Griffin", "Jared_Goff", "James_Harden", "Dirk_Nowitzki", "Steffi_Graf", "Michael_Schumacher",
				"Katarina_Witt", "Boris_Becker", "Sebastian_Vettel", "Niki_Lauda", "Fernando_Alonso", "Lewis_Hamilton", "Valentino_Rossi", "Giacomo_Agostini", "Sidney_Crosby", "Bobby_Hull",
				"Fritz_Walter", "Richie_Porte", "Dries_Devenyns", "Daryl_Impey", "Diego_Ulissi", "Brian_Lara", "Sachin_Tendulkar", "Raheem_Sterling", "Kevin_De_Bruyne", "Eden_Hazard", "Paulo_Dybala",
				"David_Alaba", "Timo_Werner", "Lorenzo_Insigne", "Romelu_Lukaku", "Bernardo_Silva", "Ada_Hegerberg", "Wendie_Renard", "Bastian_Schweinsteiger"};
	
	// Theme/Topic: Science (Person)
	public static final String[] SCIENCE_PERSON = {"Albert_Einstein", "Isaac_Newton", "Nikola_Tesla", "Galileo_Galilei", "Marie_Curie", "Peter_Higgs", "Pierre-Simon_Laplace", "André-Marie_Ampère",
			"Charles_Darwin", "Ernest_Rutherford", "Niels_Bohr", "Erwin_Schrödinger", "J._Robert_Oppenheimer", "Alan_Turing", "Richard_Feynman", "Stephen_Hawking", "Johannes_Kepler", "Pythagoras",
			"William_Harvey", "Joseph_Louis_Gay-Lussac", "Gregor_Mendel", "Robert_Koch", "Sigmund_Freud", "Alexander_Fleming", "Carl_Jung", "Max_Planck", "Donald_Knuth", "Ada_Lovelace", "Archimedes",
			"Daniel_Bernoulli", "Robert_Boyle", "Robert_Bunsen", "Nicolaus_Copernicus", "John_Dalton", "Eratosthenes", "Leonhard_Euler", "Carl_Friedrich_Gauss", "Heinrich_Hertz",
			"Edwin_Hubble", "Alfred_Nobel", "Edsger_W._Dijkstra", "Pierre_de_Fermat", "Grace_Hopper", "Joseph-Louis_Lagrange", "Amedeo_Avogadro", "Dmitri_Mendeleev", "John_von_Neumann"};

	// Theme/Topic: Politics (Person)
	public static final String[] POLITICS_PERSON = {"Xi_Jinping", "Vladimir_Putin", "Donald_Trump", "Angela_Merkel", "Emmanuel_Macron", "Theresa_May", "Kim_Jong-un", "Shinzō_Abe", "Ronald_Reagan",
			"Margaret_Thatcher", "Martin_Luther_King_Jr.", "Hideki_Tojo", "Winston_Churchill", "Adolf_Hitler", "Joseph_Stalin", "Mahatma_Gandhi",
			"Otto_von_Bismarck", "Napoleon", "Catherine_the_Great", "Genghis_Khan", "Charlemagne",
			"Nelson_Mandela", "Abraham_Lincoln", "Vladimir_Lenin", "Kim_Il-sung", "Julius_Caesar", "Alexander_the_Great", "Benjamin_Franklin", "George_Washington", "Willy_Brandt",
			"Konrad_Adenauer", "Helmut_Schmidt", "Helmut_Kohl", "Gerhard_Schröder", "Mariano_Rajoy", "José_Luis_Rodríguez_Zapatero", "George_W._Bush", "Bill_Clinton",
			"Frank-Walter_Steinmeier", "Joachim_Gauck"};

	// Theme/Topic: Music (Person)
	public static final String[] MUSIC_PERSON = {"Elvis_Presley", "Michael_Jackson", "Elton_John", "Madonna_(entertainer)", "Rihanna", "Eminem", "Taylor_Swift", "Mariah_Carey", "Whitney_Houston",
			"Celine_Dion", "Drake_(musician)", "Garth_Brooks", "Justin_Bieber", "Ed_Sheeran", "Billy_Joel", "Barbra_Streisand", "Phil_Collins", "Katy_Perry", "Kanye_West",
			"Chris_Brown", "Bruce_Springsteen", "Bruno_Mars", "Lady_Gaga", "Adele", "Lil_Wayne", "Rod_Stewart", "Beyoncé", "Nicki_Minaj", "Britney_Spears", "George_Strait", "Eric_Clapton",
			"Neil_Diamond", "Kenny_Rogers", "Paul_McCartney", "Janet_Jackson", "Julio_Iglesias", "Cher", "David_Bowie", "Stevie_Wonder", "James_Taylor", "Olivia_Newton-John", "Tina_Turner",
			"Bob_Marley", "Jimi_Hendrix", "Bob_Dylan", "Pink_(singer)", "Johnny_Cash"};

	// Theme/Topic: Music (Band)
	public static final String[] MUSIC_BAND = {"Simon_&_Garfunkel", "Earth,_Wind_&_Fire", "Red_Hot_Chili_Peppers", "Queen_(band)", "The_Beatles", "Led_Zeppelin", "Aerosmith", "Metallica", "U2",
			"Backstreet_Boys", "Eagles_(band)", "ABBA", "Maroon_5", "Fleetwood_Mac", "Bee_Gees", "Linkin_Park", "Bon_Jovi", "Chicago_(band)", "The_Beach_Boys", "The_Who",
			"The_Carpenters", "R.E.M.", "Van_Halen", "The_Doors", "Foreigner_(band)", "Journey_(band)", "The_Black_Eyed_Peas", "Nirvana_(band)",
			"Green_Day", "Alabama_(band)", "The_Police", "Kiss_(band)", "Santana_(band)", "Def_Leppard", "Genesis_(band)", "The_Velvet_Underground", "Ramones",
			"The_Clash", "Public_Enemy_(band)", "The_Band"};

	// Theme/Topic: Economy (Person)
	public static final String[] ECONOMY_PERSON = {"Bill_Gates", "Bernard_Arnault", "Warren_Buffett", "Larry_Ellison", "Mark_Zuckerberg", "Jim_Walton",
			"Mukesh_Ambani", "Larry_Page", "Elon_Musk", "Sergey_Brin", "Henry_Ford", "Andrew_Carnegie", "John_D._Rockefeller", "Carlos_Slim",
			"Michael_Bloomberg", "Jack_Ma", "Benjamin_Graham", "George_Soros",
			"David_Ricardo", "Arthur_Cecil_Pigou", "Irving_Fisher", "Charlie_Munger", "Nassim_Nicholas_Taleb"};

	// Theme/Topic: Economy (Company)
	public static final String[] ECONOMY_COMPANY = {"AT&T", "MasterCard", "Apple_Inc.", "Microsoft", "The_Walt_Disney_Company", "Alphabet_Inc.", "Berkshire_Hathaway", "Starbucks", "JPMorgan_Chase", "Costco",
			"The_Coca-Cola_Company", "Nike,_Inc.", "FedEx", "Netflix", "Walmart", "Target_Corporation", "BlackRock", "Goldman_Sachs", "Toyota", "BMW", "United_Parcel_Service", "IBM", "Visa_Inc.",
			"Nestlé", "Adidas", "Adobe_Systems", "Activision_Blizzard", "BASF", "Canon_Inc.", "Cisco_Systems", "Daimler_AG",
			"Facebook", "General_Motors", "Tesla_Motors", "Spotify", "Volkswagen_Group", "Nintendo", "Saudi_Aramco", "Tencent", "Alibaba_Group", "Intel", "Samsung_Electronics",
			"Verizon_Communications", "Banco_Santander"};
}

	
	
