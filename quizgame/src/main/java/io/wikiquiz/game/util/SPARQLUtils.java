package io.wikiquiz.game.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

public final class SPARQLUtils {
	private SPARQLUtils() {
	}

	public static final String SPARQL_ENDPOINT = "http://localhost:3333/dbpedia2016-latest/sparql";

	public static final String PREFIXES = "PREFIX schema: <http://schema.org/>"
			+ "PREFIX res: <http://dbpedia.org/resource/>" + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
			+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>" + "PREFIX dbo: <http://dbpedia.org/ontology/>"
			+ "PREFIX dbc: <http://dbpedia.org/resource/Category:>" + "PREFIX dct: <http://purl.org/dc/terms/>"
			+ "PREFIX dbp: <http://dbpedia.org/property/>";

	/**
	 * Builds query for the entityURI and its predicatesArray
	 * 
	 * @param entityURI
	 * @param predicatesArray - depends on the entity (is it of type
	 *                        person/company/band?)
	 * @return query - the built query as string
	 */
	public static String buildQuery(String entityURI, String[] predicatesArray) {
		StringBuilder optionalsStringBuilder = new StringBuilder();
		StringBuilder objectsStringBuilder = new StringBuilder();
		String query = "";

		for (int i = 0; i < predicatesArray.length; i++) {
			String predicate = predicatesArray[i];

			// remove suffix from predicate; now its a variable storing the (SPARQL-)object
			String object = "?" + predicatesArray[i].substring(4);

			objectsStringBuilder.append(object + " ");

			String appendOptional = String.format("OPTIONAL { %s %s %s }", entityURI, predicate, object);
			optionalsStringBuilder.append(appendOptional);
		}

		query = SPARQLUtils.PREFIXES + " SELECT DISTINCT " + objectsStringBuilder.toString() + "WHERE {"
				+ optionalsStringBuilder.toString() + "}";

		return query;
	}

	/**
	 * Cleans all objects (it is the value of the predicateObjectMap)
	 * 
	 * @param predicateObjectMap - the found predicates and its objects of an entity
	 * @return predicateObjectMap - the cleaned predicateObjectMap
	 */
	public static Map<String, String> cleanObjects(Map<String, String> predicateObjectMap) {
		for (Map.Entry<String, String> entry : predicateObjectMap.entrySet()) {
			String predicate = entry.getKey();
			String object = entry.getValue();

			// just extract the tokens after last occurrence of '/'
			if (object.contains("http") && !object.contains("^")) {
				object = object.substring(object.lastIndexOf("/") + 1);
				object = object.replace('_', ' ');
				predicateObjectMap.replace(predicate, object);
			}
			// just extract birthdate or height or an integer
			if (object.contains("^")) {
				object = object.substring(0, object.lastIndexOf("^") - 1);
				predicateObjectMap.replace(predicate, object);

				// if object contains '.' then it is height
				if (object.contains(".")) {
					predicateObjectMap.replace(predicate, object + " m");
				}
			}
			// just extract actual string (for example for dct:description)
			if (object.contains("@")) {
				predicateObjectMap.replace(predicate, object.substring(0, object.lastIndexOf("@")));
			}
		}

		return predicateObjectMap;
	}

	/**
	 * Applies the templates and returns the tips as list. Depends on the predicates
	 * 
	 * @param predicateObjectMap - the found predicates and its objects of an entity
	 * @param predicatesArray    - depends on the entity (is it of type
	 *                           person/company/band?)
	 * @param templatesArray     - depends on the predicates
	 * @return objectsList - list of the objects inserted in the templates
	 */
	public static List<String> applyTemplates(Map<String, String> predicateObjectMap, String[] predicatesArray,
			String[] templatesArray) {
		List<String> objectsList = new ArrayList<>();
		for (Map.Entry<String, String> entry : predicateObjectMap.entrySet()) {
			String predicate = entry.getKey();
			String object = entry.getValue();

			for (int i = 0; i < predicatesArray.length; i++) {
				if (predicate.equals(predicatesArray[i])) {
					String objectInTemplate = String.format(templatesArray[i], object);
					objectsList.add(objectInTemplate);
				}
			}
		}
		return objectsList;
	}

	/**
	 * Removes the spoiling items from the map
	 * 
	 * @param predicateObjectMap - the found predicates and its objects of an entity
	 * @param entityURI - URI of the entity
	 * @return predicateObjectMap - without the spoiling items
	 */
	public static Map<String, String> removeSpoilingItems(Map<String, String> predicateObjectMap, String entityURI) {
		String[] spoilerTokens = SPARQLUtils.filterName(entityURI);

		Iterator<Entry<String, String>> it = predicateObjectMap.entrySet().iterator();
		while (it.hasNext()) {
			Object item = it.next();
			@SuppressWarnings("unchecked")
			String object = ((Entry<String, String>) item).getValue();

			if (SPARQLUtils.isSpoiler(object, spoilerTokens)) {
				it.remove();
			}
		}
		return predicateObjectMap;
	}

	/**
	 * Returns true if a element is probably spoiling the right answer; otherwise false
	 * 
	 * @param object - the object which is checked if it is spoiling the winning entity
	 * @param tokens - are all parts of an entity name
	 * @return true if it spoiling; otherwise false
	 */
	public static boolean isSpoiler(String object, String[] tokens) {
		for (int i = 0; i < tokens.length; i++) {
			if (object.contains(tokens[i])) {
				return true;
			}
		}
		return false;
	}

	/**
	 *  Replace the entityURI with its cleaned name
	 * 
	 * @param map - entityURI and its image
	 * @return replacedMap - map with the cleaned names
	 */
	public static Map<String, String> replaceEntityURI(Map<String, String> map) {
		Map<String, String> replacedMap = new HashMap<String, String>();
		for (Map.Entry<String, String> entry : map.entrySet()) {
			String entityURI = entry.getKey();
			String imageURI = entry.getValue();

			String name = entityURI.substring(entityURI.lastIndexOf("/") + 1).replace(">", "").replace("_", " ");
			replacedMap.put(name, imageURI);
		}

		return replacedMap;
	}

	/**
	 * Select an entity from the array randomly
	 * 
	 * @param entityArray - defined array with many entities
	 * @return random entity from the passed array
	 */
	public static String chooseRandomlyEntity(String[] entityArray) {
		Random random = new Random();
		return entityArray[random.nextInt(entityArray.length)];
	}

	/**
	 * Shuffle the passed map
	 * 
	 * @param map
	 * @return shuffledMap - the shuffled version of map
	 */
	public static Map<String, String> shuffleMap(Map<String, String> map) {
		Set<String> s = map.keySet();
		List<String> keysList = new ArrayList<String>(s);
		System.out.println("SPARQLUtils shuffleMap() keysList: " + keysList);
		Collections.shuffle(keysList);
		Map<String, String> shuffledMap = new LinkedHashMap<String, String>();

		int i = 0;
		while (i < 4) {
			String key = keysList.get(i);
			shuffledMap.put(key, map.get(key));
			i++;
		}
		return shuffledMap;
	}

	/**
	 * 	Returns an array with all parts of the full name
	 * 
	 * @param entityURI
	 * @return array with all parts of the full name
	 */
	public static String[] filterName(String entityURI) {
		String name = entityURI.substring(entityURI.lastIndexOf("/") + 1).replace(">", "");
		return name.split("_");
	}
	
	
	
	
	
	
	// ***********************************************************************************
	//              DUE TO BETTER IMPLEMENTATION IDEAS NOT NEEDED ANYMORE
	// ***********************************************************************************
	
	// filters the subject name in every item from list (removes whole URI except
	// the subject name and replaces '_' with whitespace)
	public static List<String> cleanSubjects(List<String> subjectsList) {
		for (int i = 0; i < subjectsList.size(); i++) {
			String rawSubject = subjectsList.get(i);

			// remove the prefix until ':'
			String subject = rawSubject.substring(rawSubject.lastIndexOf(":") + 1);

			// replace '_' with whitespace
			subject = subject.replace('_', ' ');

			subjectsList.set(i, subject);
		}

		return subjectsList;
	}

	// removes unnecessary subjects (the sentence of the sentence is simpler) For
	// example: 'This boxer and People from X.'. People is plural but 'This' refers
	// to 1 athlete.
	public static List<String> removeUnnecessarySubjects(List<String> subjectsList) {
		// if subject contains a unnecessary word then remove subject from list
		List<String> unnecessaryTokensList = Arrays.asList("people", "birth");

		for (int i = 0; i < subjectsList.size(); i++) {
			String subject = subjectsList.get(i).toLowerCase();

			for (int j = 0; j < unnecessaryTokensList.size(); j++) {
				String unnecessaryToken = unnecessaryTokensList.get(j);

				if (subject.contains(unnecessaryToken)) {
					subjectsList.remove(i);
				}
			}
		}

		return subjectsList;
	}

	// singularizes the subjects (Person)
	// actually not sure if we should put these three singularizeSubjectX()
	// functions into one function, currently they are almost similar (copy paste).
	// But because of readability or future modifications/specializations, these
	// functions should be separated?!)
	public static List<String> singularizeSubjectsPerson(List<String> rawSubjectList) {
		// list with words which should get replaced with their singular version. List
		// is not completed, many words are still missing
		List<String> wordsToReplace = Arrays.asList("players", "forwards", "Catholics", "expatriates", "Americans",
				"guards", "Medalists", "medalists", "picks", "All-Stars", "footballers", "Officers", "births",
				"champions", "boxers", "Cricketers", "cricketers", "captains", "Captains", "Recipients", "Members");
		// clean subjects before replacing words
		List<String> cleanedSubjectsList = SPARQLUtils.cleanSubjects(rawSubjectList);
		String singularizedSubject = "";
		String wordToReplace = "";

		for (int i = 0; i < cleanedSubjectsList.size(); i++) {
			String subject = cleanedSubjectsList.get(i);

			// check if subject contains a word of the list
			for (int j = 0; j < wordsToReplace.size(); j++) {
				wordToReplace = wordsToReplace.get(j);

				if (subject.contains(wordToReplace)) {
					singularizedSubject = subject.replace(wordToReplace,
							wordToReplace.substring(0, wordToReplace.length() - 1));
					cleanedSubjectsList.set(i, singularizedSubject);
				}
			}
		}
		return cleanedSubjectsList;
	}

	// singularizes the subjects (Company)
	public static List<String> singularizeSubjectsCompany(List<String> rawSubjectList) {
		// list with words which should get replaced with their singular version. List
		// is not completed, many words are still missing
		List<String> wordsToReplace = Arrays.asList("companies", "providers", "establishments", "manufacturers",
				"websites", "systems", "properties", "services");

		// clean subjects before replacing words
		List<String> cleanedSubjectsList = SPARQLUtils.cleanSubjects(rawSubjectList);
		String singularizedSubject = "";
		String wordToReplace = "";

		for (int i = 0; i < cleanedSubjectsList.size(); i++) {
			String subject = cleanedSubjectsList.get(i);

			// check if subject contains a word of the list
			for (int j = 0; j < wordsToReplace.size(); j++) {
				wordToReplace = wordsToReplace.get(j);

				if (subject.contains(wordToReplace)) {
					if (wordToReplace.equals("companies") || wordToReplace.equals("properties")) {
						singularizedSubject = subject.replace(wordToReplace,
								wordToReplace.substring(0, wordToReplace.length() - 3) + "y");
						cleanedSubjectsList.set(i, singularizedSubject);
					} else {
						singularizedSubject = subject.replace(wordToReplace,
								wordToReplace.substring(0, wordToReplace.length() - 1));
						cleanedSubjectsList.set(i, singularizedSubject);
					}
				}
			}
		}
		return cleanedSubjectsList;
	}

	// singularizes the subjects (Band)
	public static List<String> singularizeSubjectsBand(List<String> rawSubjectList) {
		// list with words which should get replaced with their singular version. List
		// is not completed, many words are still missing
		List<String> wordsToReplace = Arrays.asList("groups", "winners", "inductees");

		// clean subjects before replacing words
		List<String> cleanedSubjectsList = SPARQLUtils.cleanSubjects(rawSubjectList);
		String singularizedSubject = "";
		String wordToReplace = "";

		for (int i = 0; i < cleanedSubjectsList.size(); i++) {
			String subject = cleanedSubjectsList.get(i);

			// check if subject contains a word of the list
			for (int j = 0; j < wordsToReplace.size(); j++) {
				wordToReplace = wordsToReplace.get(j);

				if (subject.contains(wordToReplace)) {
					singularizedSubject = subject.replace(wordToReplace,
							wordToReplace.substring(0, wordToReplace.length() - 1));
					cleanedSubjectsList.set(i, singularizedSubject);
				}
			}
		}
		return cleanedSubjectsList;
	}

	// generate sentence with the help of subjects (and maybe with types soon?)
	public static String generateSentence(List<String> subjectsList) {
		StringBuilder sentenceStringBuilder = new StringBuilder("This ");
		int maxNumber = 3;
		Random random = new Random();

		// choose randomly 3 subjects
		for (int i = 0; i < maxNumber; i++) {
			int randomNumber = random.nextInt(subjectsList.size());

			if (i != maxNumber - 1) {
				sentenceStringBuilder.append(subjectsList.get(randomNumber) + " and ");
			} else {
				sentenceStringBuilder.append(subjectsList.get(randomNumber) + ".");
			}
		}

		return sentenceStringBuilder.toString();
	}
}
