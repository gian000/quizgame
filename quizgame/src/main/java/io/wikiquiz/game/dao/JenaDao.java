package io.wikiquiz.game.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;

import io.wikiquiz.game.util.SPARQLUtils;

/**
 * 
 * JenaDao class - Database layer
 * Fires queries on our Apache Jena server
 *
 */
public class JenaDao {
	
    /**
     * Finds all available predicates of an entity. Depends on the predicatesArray
     * 
     * @param query - the built query
     * @param predicatesArray
     * @return predicatesMap - map with all the predicates and its objects
     */
    public Map<String, String> findPredicates(String query, String[] predicatesArray) {
        Map<String, String> predicatesMap = new HashMap<String, String>();
    	QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        if(results.hasNext()) {
        	QuerySolution querySolution = results.next();

        	for (int i = 0; i < predicatesArray.length; i++) {
            	String predicate = predicatesArray[i];
                String object = predicatesArray[i].substring(4);
                
                RDFNode objectFromResult = querySolution.get(object);
                
                // if RDFNode is null, then predicate does not exist for this entity
                if(objectFromResult != null) {
                	predicatesMap.put(predicate, objectFromResult.toString());
                }
            }
        }
        queryExecution.close(); // close connection, important
        return predicatesMap;
    }
    
    /**
     * Counts all rdf:type (which their object contain 'Wikicat') of an entity
     * 
     * @param entity - entityURI
     * @return count - count of types
     */
    public int countRdfTypes(String entity) {
    	String query =
    			SPARQLUtils.PREFIXES
                + "SELECT (COUNT(?type) AS ?count) "
                + "WHERE { "
                + entity + " rdf:type ?type. "
                + "FILTER(CONTAINS(STR(?type), \"Wikicat\")). "
                + "}";

    	 QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
         ResultSet results = queryExecution.execSelect();
         
         int count = 0;
         if(results.hasNext()) {
         	QuerySolution querySolution = results.next();
        	Literal getCount = (Literal) querySolution.get("count"); // count is ?count in query
        	count = getCount.getInt();
         }
         
         queryExecution.close(); // close connection, important
         
        return count;
    }
    
    // QueryExecution should be closed.
    // Otherwise too many connections remain open.
    // (the error you mentioned: all connections are busy; the app waits until a connection is free)
    // (currently 5 http connections can be open simultaneously)
    // the problem is: if you close QueryExecution, you close the ResultSet too.
    // Thats the reason why here is returned a HashMap/Collection and not ResultSet.
    /**
     * Counts for each other entity how many rdf:type are in common with the target entity
     * 
     * @param entity
     * @return resourcesMap
     */
    public Map<String, Integer> countSimilarRdfTypes(String entity) {
        String query = SPARQLUtils.PREFIXES +

                "SELECT ?resource ?image (COUNT(?resource) AS ?count) "
                + "WHERE { "
                + entity + " rdf:type ?type. "
                + "FILTER(CONTAINS(STR(?type), \"Wikicat\")). "
                + "?resource rdf:type ?type."
                + "?resource dbo:thumbnail ?image. "
                + "} GROUP BY ?resource ?image ORDER BY DESC(?count) LIMIT 5";
        
        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        Map<String, Integer> resourcesMap = new HashMap<String, Integer>(); 
           
        // iterate through ResultSet and put every element into HashMap
        while(results.hasNext()) {        	
        	QuerySolution querySolution = results.next();
        	String resource = "<" + querySolution.get("resource").toString() + ">"; // resource is ?resource in query
        	Literal getCount = (Literal) querySolution.get("count"); // count is ?count in query
        	int count = getCount.getInt();
        	
        	resourcesMap.put(resource, count);
        }
        
        // make sure that the target entity is not present in hash map
        // sometimes the target entity is not the first entity and performing .next() would not help to remove target entity.
        // (I tried an example with res:Canicattì. The resource had the 3rd place, with .next() the wrong entity got removed)
       // String entityWithoutPrefix = entity.substring(4, entity.length());
        resourcesMap.remove(entity);
        
        queryExecution.close(); // close connection, important
        
        return resourcesMap;
    }

    /**
     * Finds the 3 most similar entities
     * 
     * @param targetEntityURI
     * @return similarEntitiesList - list with the 3 most similar entities
     */
    public List<String> findSimilarEntities(String targetEntityURI) {

        // init HashMap that contains the count of in common rdf:type between
        // the target entity and each other entity
        Map<String, Integer> resourcesMap = countSimilarRdfTypes(targetEntityURI);

        //initialize the variables that contain the count of how many rdf:type the entity has
        double typeCountTargetEntity = countRdfTypes(targetEntityURI);
        double typeCountNextEntity;

        //initialize the variables that contain the jaccard coefficient
        double jacCoef = 0.0;
        double jacCoef1 = 0.0;
        double jacCoef2 = 0.0;
        double jacCoef3 = 0.0;

        //initialize the similar entity strings
        String similarEntity1 = "";
        String similarEntity2 = "";
        String similarEntity3 = "";

        //this loop runs through all entities with at least one common rdf:type
        for (Map.Entry<String, Integer> entry : resourcesMap.entrySet()) {
        	String resource = entry.getKey();
            Integer count = entry.getValue();

            String nextEntity = resource;
            double nextEntityInterceptionCount = count;

            //get the count of rdf:type of the entity
            typeCountNextEntity = countRdfTypes(nextEntity);
            
            //compute the jaccard coefficient
            jacCoef = nextEntityInterceptionCount / (typeCountTargetEntity + typeCountNextEntity - nextEntityInterceptionCount);

            //compare if the jaccard coefficient is high enough
            if(jacCoef > jacCoef1) {
                jacCoef3 = jacCoef2;
                jacCoef2 = jacCoef1;
                jacCoef1 = jacCoef;
                similarEntity3 = similarEntity2;
                similarEntity2 = similarEntity1;
                similarEntity1 = nextEntity;
            } else if(jacCoef > jacCoef2) {
                jacCoef3 = jacCoef2;
                jacCoef2 = jacCoef;
                similarEntity3 = similarEntity2;
                similarEntity2 = nextEntity;
            } else if(jacCoef > jacCoef3) {
                jacCoef3 = jacCoef;
                similarEntity3 = nextEntity;
            }
        }

        // add the 3 most similar entities to list
        List<String> similarEntitiesList = new ArrayList<String>();
        similarEntitiesList.add(similarEntity1);
        similarEntitiesList.add(similarEntity2);
        similarEntitiesList.add(similarEntity3);
        return similarEntitiesList;
    }
    
    /**
     * Finds images of the similar entities and return map (entity, image)
     * 
     * @param similarEntitiesList
     * @return entityImageMap
     */
    public Map<String, String> findImages(List<String> similarEntitiesList) {
    	Map<String, String> entityImageMap = new HashMap<String, String>();
    	String imageURI = "";
    	String query;
    	
    	for(int i = 0; i < similarEntitiesList.size(); i++) {
    		String entityURI = similarEntitiesList.get(i);
        	query = SPARQLUtils.PREFIXES +
        			"SELECT ?image " +
        			"WHERE {" +
        			entityURI + " dbo:thumbnail" + " ?image." +
        			"}";
    		QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
    		ResultSet results = queryExecution.execSelect();
    		
    		// get image URI
    		if(results.hasNext()) {
    			QuerySolution querySolution = results.next();
    			
    			imageURI = querySolution.get("image").toString();
    				
    			entityImageMap.put(entityURI, imageURI);
    		}
    		
    		queryExecution.close();
    	}

		return entityImageMap;
    }
    
    
    
    
    
	// ***********************************************************************************
	//              DUE TO BETTER IMPLEMENTATION IDEAS NOT NEEDED ANYMORE
	// ***********************************************************************************
    
    //get all predicates (and their objects) of an entity
    public Map<String, String> findAllPredicates(String entity) {
        String query = SPARQLUtils.PREFIXES +
                "SELECT ?type ?object WHERE { " +
                entity + " ?type ?object." +
                "}";

        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        Map<String, String> predicatesMap = new HashMap<String, String>();
        
        while(results.hasNext()) {
        	QuerySolution querySolution = results.next();
        	String type = querySolution.get("type").toString();
        	String object = querySolution.get("object").toString();
        	
        	// get predicate name of URI
        	String predicateName = type.substring(type.lastIndexOf("/") + 1);
        	
        	predicatesMap.put(predicateName, object);
        }
        
        queryExecution.close(); // close connection, important
        
        return predicatesMap;
    }

    //get all subjects of an entity
    public List<String> findAllSubjects(String entity) {
        String query = SPARQLUtils.PREFIXES +

                "SELECT ?subject WHERE { " +
                entity + " dct:subject ?subject." +
                "}";

        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        List<String> subjectList = new ArrayList<String>();
        
        while(results.hasNext()) {
        	QuerySolution querySolution = results.next();
        	String subject = querySolution.get("subject").toString();
        	
        	subjectList.add(subject);
        }
        
        queryExecution.close(); // close connection, important
        
        return subjectList;
    }
    
	// finds a random person with specific types (these specific types define the topic/theme)
	// specific types are defined in SPARQLUtils.java
	public String findRandomPerson(String types) {		 
		String entity = "";
		String[] typesSplitted = types.split("\\s+");
		StringBuilder queryStringBuilder;
		List<String> entities  = new ArrayList<>();
		
		String headQuery = SPARQLUtils.PREFIXES
				+ "SELECT ?subject " +
				"WHERE {";
		
		queryStringBuilder = new StringBuilder(headQuery);

		// add all rdf types to query
		for(int i = 0; i < typesSplitted.length; i++) {
			queryStringBuilder.append("?subject rdf:type " + typesSplitted[i] + ".");
		}
		
		String footerQuery =
				"?subject dbo:thumbnail ?image." +
				"} LIMIT 1000";
		
		queryStringBuilder.append(footerQuery);
		
		QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, queryStringBuilder.toString());
		ResultSet results = queryExecution.execSelect();
		
		// add all persons to list
		while(results.hasNext()) {
			QuerySolution querySolution = results.next();
			entity = querySolution.get("subject").toString();
			entities.add(entity);
		}

		// create random number
		Random random = new Random();
		int randomNumber = random.nextInt(entities.size());
		
		// get random entity from list
		entity = entities.get(randomNumber);
		
		queryExecution.close();
		
		return "<" + entity + ">";
	}
	
    //get all award of an entity
    public List<String> findAllAwards(String entity) {
        String query = SPARQLUtils.PREFIXES +

                "SELECT ?award WHERE { " +
                entity + " dbo:award ?award." +
                "}";

        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        List<String> awardList = new ArrayList<>();

        while(results.hasNext()) {
            QuerySolution querySolution = results.next();
            String award = querySolution.get("award").toString();

            awardList.add(award);
        }

        queryExecution.close();

        return awardList;
    }

    //get all things the entity is known for(not that many entities have that)
    public List<String> findAllKnownFor(String entity) {
        String query = SPARQLUtils.PREFIXES +

                "SELECT ?knownFor WHERE { " +
                entity + " dbo:knownFor ?knownFor." +
                "}";

        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        List<String> knownForList = new ArrayList<>();

        while(results.hasNext()) {
            QuerySolution querySolution = results.next();
            String knownFor = querySolution.get("knownFor").toString();

            knownForList.add(knownFor);
        }

        queryExecution.close();

        return knownForList;
    }

    //get the birthdate of an entity
    public String findBirthDate(String entity) {
        String query = SPARQLUtils.PREFIXES +

                "SELECT ?birthDate WHERE { " +
                entity + " dbo:birthDate ?birthDate." +
                "}";

        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        String birthDate = "";

        while(results.hasNext()) {
            QuerySolution querySolution = results.next();
            birthDate = querySolution.get("birthDate").toString();
        }

        queryExecution.close();

        String[] s1 = birthDate.split("h");
        String[] s2 = s1[0].split("-");
        String birthDateFormatted = s2[2].substring(0, s2[2].length()-2) + "." + s2[1] + "." + s2[0];

        return birthDateFormatted;
    }

    //get the birthplace of an entity
    public String findBirthPlace(String entity) {
        String query = SPARQLUtils.PREFIXES +

                "SELECT ?birthPlace WHERE { " +
                entity + " dbo:birthPlace ?birthPlace." +
                "}";

        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        String birthPlace = "";

        while(results.hasNext()) {
            QuerySolution querySolution = results.next();
            birthPlace = querySolution.get("birthPlace").toString();
        }

        queryExecution.close();

        String[] s = birthPlace.split("/");
        String birthPlaceFormatted = s[4];
        birthPlaceFormatted = birthPlaceFormatted.replace('_', ' ');


        return birthPlaceFormatted;
    }

    //get the height of an entity
    public String findHeight(String entity) {
        String query = SPARQLUtils.PREFIXES +

                "SELECT ?height WHERE { " +
                entity + " dbo:height ?height." +
                "}";

        QueryExecution queryExecution = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = queryExecution.execSelect();

        String height = "";

        while(results.hasNext()) {
            QuerySolution querySolution = results.next();
            height = querySolution.get("height").toString();
        }

        queryExecution.close();

        if(height.length() > 4) {
            height = height.substring(0, 4);
        }

        return height + " m";
    }
    
    //prints the results of the query
    public void execSelectAndPrint(String query) {
    	QueryExecution q = QueryExecutionFactory.sparqlService(SPARQLUtils.SPARQL_ENDPOINT, query);
        ResultSet results = q.execSelect();

        ResultSetFormatter.out(System.out, results);
        q.close();
    }
}
