package io.wikiquiz.game.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import io.wikiquiz.game.model.GameMultiplayerSession;
import org.springframework.stereotype.Service;
import io.wikiquiz.game.dao.JenaDao;
import io.wikiquiz.game.model.Entities;
import io.wikiquiz.game.util.SPARQLUtils;
import io.wikiquiz.game.util.StringUtils;

/**
 * 
 * GameService class for multiplayer mode
 *
 */
@Service
public class GameMultiplayerService {
	@Resource(name = "gameMultiplayerSession")
	private GameMultiplayerSession gameMultiplayerSession;

	private JenaDao jenaDao;

	public GameMultiplayerService() {
		jenaDao = new JenaDao();
	}

	/**
	 * Generates the tips
	 * 
	 * @param entityURI       - the URI of the entity
	 * @param predicatesArray - the array with predicates which depends on the
	 *                        entity
	 * @param templatesArray  - the array with templates (for the predicates) which
	 *                        depends on the entity
	 * @return generatedTips - A list with all the fetched and processed tips
	 */
	public List<String> generateTips(String entityURI, String[] predicatesArray, String[] templatesArray) {
		// build query
		String query = SPARQLUtils.buildQuery(entityURI, predicatesArray);

		// find all available predicates and its objects of the entityURI
		Map<String, String> foundPredicateObjectMap = jenaDao.findPredicates(query, predicatesArray);

		// clean the objects
		SPARQLUtils.cleanObjects(foundPredicateObjectMap);

		// remove spoiling items
		SPARQLUtils.removeSpoilingItems(foundPredicateObjectMap, entityURI);

		// apply templates
		List<String> generatedTips = SPARQLUtils.applyTemplates(foundPredicateObjectMap, predicatesArray,
				templatesArray);

		// shuffle list; otherwise the tips in same order are shown
		Collections.shuffle(generatedTips);
		return generatedTips;
	}

	/**
	 * Finds the images of the winning entity and its similar entities with their
	 * names (cleaned) as map
	 * 
	 * @param targetEntityURI - the winning entity
	 * @return replacedMap - map(entityname, url-image)
	 */
	public Map<String, String> findImages(String targetEntityURI) {
		// find similar entities
		List<String> similarEntitiesList = jenaDao.findSimilarEntities(targetEntityURI);

		// if similarEntitiesList has an empty value then return an empty map. (So we
		// can try this with a new winning entity)
		if (similarEntitiesList.contains("")) {
			return Collections.emptyMap();
		}

		// add the winning entity to the map
		similarEntitiesList.add(targetEntityURI);

		// find the images itself
		Map<String, String> entityImageMap = jenaDao.findImages(similarEntitiesList);

		// replace the entityURI's with their cleaned entity name
		Map<String, String> replacedMap = SPARQLUtils.replaceEntityURI(entityImageMap);

		// shuffle the map
		replacedMap = SPARQLUtils.shuffleMap(replacedMap);
		return replacedMap;
	}

	/**
	 * Depending on the selected theme and entity type it returns the right entities
	 * object and sets the right question
	 * 
	 * @param selectedTheme
	 * @param selectedEntityType
	 * @return rightEntities - the right entities object
	 */
	public Entities findRightEntities(String selectedTheme, String selectedEntityType) {
		Entities rightEntities = null;

		if (selectedTheme.equalsIgnoreCase("Sports")) {
			rightEntities = new Entities(StringUtils.SPORTS_PERSON, StringUtils.PERSON_PREDICATES,
					StringUtils.PERSON_TEMPLATES);
			gameMultiplayerSession.setQuestion("Which athlete is it?");

		} else if (selectedTheme.equalsIgnoreCase("Science")) {
			rightEntities = new Entities(StringUtils.SCIENCE_PERSON, StringUtils.PERSON_PREDICATES,
					StringUtils.PERSON_TEMPLATES);
			gameMultiplayerSession.setQuestion("Which scientist is it?");

		} else if (selectedTheme.equalsIgnoreCase("Politics")) {
			rightEntities = new Entities(StringUtils.POLITICS_PERSON, StringUtils.PERSON_PREDICATES,
					StringUtils.PERSON_TEMPLATES);
			gameMultiplayerSession.setQuestion("Which politician is it?");

		} else if (selectedTheme.equalsIgnoreCase("Music")) {

			if (selectedEntityType.equalsIgnoreCase("Person")) {
				rightEntities = new Entities(StringUtils.MUSIC_PERSON, StringUtils.PERSON_PREDICATES,
						StringUtils.PERSON_TEMPLATES);
				gameMultiplayerSession.setQuestion("Which musician is it?");

			} else if (selectedEntityType.equalsIgnoreCase("Band")) {
				rightEntities = new Entities(StringUtils.MUSIC_BAND, StringUtils.BAND_PREDICATES,
						StringUtils.BAND_TEMPLATES);
				gameMultiplayerSession.setQuestion("Which band is it?");
			}

		} else if (selectedTheme.equalsIgnoreCase("Economy")) {

			if (selectedEntityType.equalsIgnoreCase("Person")) {
				rightEntities = new Entities(StringUtils.ECONOMY_PERSON, StringUtils.PERSON_PREDICATES,
						StringUtils.PERSON_TEMPLATES);
				gameMultiplayerSession.setQuestion("Which economist is it?");

			} else if (selectedEntityType.equalsIgnoreCase("Company")) {
				rightEntities = new Entities(StringUtils.ECONOMY_COMPANY, StringUtils.COMPANY_PREDICATES,
						StringUtils.COMPANY_TEMPLATES);
				gameMultiplayerSession.setQuestion("Which company is it?");
			}
		}

		return rightEntities;
	}

	/**
	 * Starts the multiplayer game
	 */
	public void initGame() {
		if (!gameMultiplayerSession.isPlaying()) {
			Entities foundRightEntities = findRightEntities(gameMultiplayerSession.getSelectedTheme(),
					gameMultiplayerSession.getSelectedEntityType()); // find right entities (it depends from the
																		// selection of the player in game-settings)
			String[] foundRightEntityArray = foundRightEntities.getEntityArray();
			String winningEntity = SPARQLUtils.chooseRandomlyEntity(foundRightEntityArray);
			String winningEntityURI = "<http://dbpedia.org/resource/" + winningEntity + ">";
			Map<String, String> entityImageMap = findImages(winningEntityURI);
			List<String> generatedTips = generateTips(winningEntityURI, foundRightEntities.getPredicateArray(),
					foundRightEntities.getTemplateArray());

			gameMultiplayerSession.setRound(0);
			gameMultiplayerSession.setCountTip(1);
			gameMultiplayerSession.getPlayer1().setPoints(0);
			gameMultiplayerSession.getPlayer1().setPoints(0);
			gameMultiplayerSession.setSelectingPlayer(1);
			gameMultiplayerSession.setPlaying(true);
			gameMultiplayerSession.setSelected(false);
			gameMultiplayerSession.setRightSelection(false);
			gameMultiplayerSession.setWinningEntity(winningEntity.replace("_", " "));
			gameMultiplayerSession.setTipsList(generatedTips);
			gameMultiplayerSession.setEntityImageMap(entityImageMap);
		}
	}

	/**
	 * Starts the next round
	 */
	public void nextRound() {
		if (gameMultiplayerSession.getRound() <= 6) {
			Entities foundRightEntities = findRightEntities(gameMultiplayerSession.getSelectedTheme(),
					gameMultiplayerSession.getSelectedEntityType()); // find right entities (it depends from the
																		// selection of the player in game-settings)
			String[] foundRightEntityArray = foundRightEntities.getEntityArray();
			String winningEntity = SPARQLUtils.chooseRandomlyEntity(foundRightEntityArray);
			String winningEntityURI = "<http://dbpedia.org/resource/" + winningEntity + ">";
			Map<String, String> entityImageMap = findImages(winningEntityURI);
			List<String> generatedTips = generateTips(winningEntityURI, foundRightEntities.getPredicateArray(),
					foundRightEntities.getTemplateArray());

			if (gameMultiplayerSession.getSelectingPlayer() == 1) {
				gameMultiplayerSession.setSelectingPlayer(2);
			} else {
				gameMultiplayerSession.setSelectingPlayer(1);
			}

			gameMultiplayerSession.setCountTip(1);
			gameMultiplayerSession.setRightSelection(false);
			gameMultiplayerSession.setSelected(false);
			gameMultiplayerSession.setWinningEntity(winningEntity.replace("_", " "));
			gameMultiplayerSession.setTipsList(generatedTips);
			gameMultiplayerSession.setEntityImageMap(entityImageMap);
		}
	}

	/**
	 * Evaluates the selected entity
	 * 
	 * @param selectedEntity
	 * @return evaluation as string
	 */
	public String selectEntity(String selectedEntity) {
		gameMultiplayerSession.increaseRound();

		if (gameMultiplayerSession.getRound() >= 6) {
			gameMultiplayerSession.setPlaying(false);
		}

		return gameMultiplayerSession.evaluateSelection(selectedEntity);
	}

	/**
	 * Increased counTip by 1
	 */
	public void increaseCountTip() {
		gameMultiplayerSession.increaseCountTip();
	}
}