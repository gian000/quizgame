package io.wikiquiz.game.service;

import javax.annotation.Resource;

import io.wikiquiz.game.model.GameMultiplayerSession;
import org.springframework.stereotype.Service;

import io.wikiquiz.game.model.GameSession;
import io.wikiquiz.game.util.StringUtils;

/**
 * 
 * GameSettingsService class for the singleplayer mode and multiplayer mode
 *
 */
@Service
public class GameSettingsService {

	@Resource(name = "gameSession")
	private GameSession gameSession;

	@Resource(name = "gameMultiplayerSession")
	private GameMultiplayerSession gameMultiplayerSession;

	public GameSettingsService() {
	}

	public String[] findThemes() {
		return StringUtils.THEMES;
	}

	public String[] findEntityTypes() {
		return StringUtils.ENTITY_TYPES;
	}

	/**
	 * Resets the singleplayer mode
	 */
	public void resetSettings() {
		gameSession.reset();
	}

	/**
	 * Apply selected singleplayer settings. Set the settings into gameSession.
	 * 
	 * @param selectedEntityType
	 * @param selectedTheme
	 * @param selectedMode
	 * @param player1Name
	 */
	public void applySettings(String selectedEntityType, String selectedTheme, String selectedMode,
			String player1Name) {
		gameSession.setSelectedEntityType(selectedEntityType.toUpperCase());
		gameSession.setSelectedTheme(selectedTheme.toUpperCase());
		gameSession.setSelectedMode(selectedMode.toUpperCase());
		gameSession.getPlayer().setName(player1Name);
	}

	/**
	 * Resets the multiplayer settings
	 */
	public void resetMultiplayerSettings() {
		gameMultiplayerSession.reset();
	}

	/**
	 * Apply selected multiplayer settings. Set the settings into
	 * gameMultiplayerSession
	 * 
	 * @param selectedEntityType
	 * @param selectedTheme
	 * @param selectedMode
	 * @param player1Name
	 * @param player2Name
	 */
	public void applyMultiplayerSettings(String selectedEntityType, String selectedTheme, String selectedMode,
			String player1Name, String player2Name) {
		System.out.println("GameSettingsService applyMultiplayerSettings()");
		gameMultiplayerSession.setSelectedEntityType(selectedEntityType.toUpperCase());
		gameMultiplayerSession.setSelectedTheme(selectedTheme.toUpperCase());
		gameMultiplayerSession.setSelectedMode(selectedMode.toUpperCase());
		gameMultiplayerSession.getPlayer1().setName(player1Name);
		gameMultiplayerSession.getPlayer2().setName(player2Name);
	}
}
