package io.wikiquiz.game.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import io.wikiquiz.game.dao.JenaDao;
import io.wikiquiz.game.model.Entities;
import io.wikiquiz.game.model.GameSession;
import io.wikiquiz.game.util.SPARQLUtils;
import io.wikiquiz.game.util.StringUtils;

/**
 * 
 * GameService class for singleplayer mode
 *
 */
@Service
public class GameService {
	@Resource(name = "gameSession")
	private GameSession gameSession;

	private JenaDao jenaDao;

	public GameService() {
		jenaDao = new JenaDao();
	}

	/**
	 * Generates the tips
	 * 
	 * @param entityURI       - the URI of the entity
	 * @param predicatesArray - the array with predicates which depends on the
	 *                        entity
	 * @param templatesArray  - the array with templates (for the predicates) which
	 *                        depends on the entity
	 * @return generatedTips - A list with all the fetched and processed tips
	 */
	public List<String> generateTips(String entityURI, String[] predicatesArray, String[] templatesArray) {
		// build query
		String query = SPARQLUtils.buildQuery(entityURI, predicatesArray);

		// find all available predicates and its objects of the entityURI
		Map<String, String> foundPredicateObjectMap = jenaDao.findPredicates(query, predicatesArray);

		// clean the objects
		SPARQLUtils.cleanObjects(foundPredicateObjectMap);

		// remove spoiling items
		SPARQLUtils.removeSpoilingItems(foundPredicateObjectMap, entityURI);

		// apply templates
		List<String> generatedTips = SPARQLUtils.applyTemplates(foundPredicateObjectMap, predicatesArray,
				templatesArray);

		// shuffle list; otherwise the tips in same order are shown
		Collections.shuffle(generatedTips);
		return generatedTips;
	}

	/**
	 * Finds the images of the winning entity and its similar entities with their
	 * names (cleaned) and returns a map
	 * 
	 * @param targetEntityURI - the winning entity
	 * @return replacedMap - map(entityname, url-image)
	 */
	public Map<String, String> findImages(String targetEntityURI) {
		// find similar entities
		List<String> similarEntitiesList = jenaDao.findSimilarEntities(targetEntityURI);

		// if similarEntitiesList has an empty value then return an empty map. (So we
		// can try this with a new winning entity)
		if (similarEntitiesList.contains("")) {
			return Collections.emptyMap();
		}

		// add the winning entity to the map
		similarEntitiesList.add(targetEntityURI);

		// find the images itself
		Map<String, String> entityImageMap = jenaDao.findImages(similarEntitiesList);

		// replace the entityURI's with their cleaned entity name
		Map<String, String> replacedMap = SPARQLUtils.replaceEntityURI(entityImageMap);

		// shuffle the map
		replacedMap = SPARQLUtils.shuffleMap(replacedMap);
		return replacedMap;
	}

	/**
	 * Depending on the selected theme and entity type it returns the right entities
	 * object and sets the right question
	 * 
	 * @param selectedTheme
	 * @param selectedEntityType
	 * @return rightEntities - the right entities object
	 */
	public Entities findRightEntities(String selectedTheme, String selectedEntityType) {
		Entities rightEntities = null;

		if (selectedTheme.equalsIgnoreCase("Sports")) {
			rightEntities = new Entities(StringUtils.SPORTS_PERSON, StringUtils.PERSON_PREDICATES,
					StringUtils.PERSON_TEMPLATES);
			gameSession.setQuestion("Which athlete is it?");

		} else if (selectedTheme.equalsIgnoreCase("Science")) {
			rightEntities = new Entities(StringUtils.SCIENCE_PERSON, StringUtils.PERSON_PREDICATES,
					StringUtils.PERSON_TEMPLATES);
			gameSession.setQuestion("Which scientist is it?");

		} else if (selectedTheme.equalsIgnoreCase("Politics")) {
			rightEntities = new Entities(StringUtils.POLITICS_PERSON, StringUtils.PERSON_PREDICATES,
					StringUtils.PERSON_TEMPLATES);
			gameSession.setQuestion("Which politician is it?");

		} else if (selectedTheme.equalsIgnoreCase("Music")) {

			if (selectedEntityType.equalsIgnoreCase("Person")) {
				rightEntities = new Entities(StringUtils.MUSIC_PERSON, StringUtils.PERSON_PREDICATES,
						StringUtils.PERSON_TEMPLATES);
				gameSession.setQuestion("Which musician is it?");

			} else if (selectedEntityType.equalsIgnoreCase("Band")) {
				rightEntities = new Entities(StringUtils.MUSIC_BAND, StringUtils.BAND_PREDICATES,
						StringUtils.BAND_TEMPLATES);
				gameSession.setQuestion("Which band is it?");
			}

		} else if (selectedTheme.equalsIgnoreCase("Economy")) {

			if (selectedEntityType.equalsIgnoreCase("Person")) {
				rightEntities = new Entities(StringUtils.ECONOMY_PERSON, StringUtils.PERSON_PREDICATES,
						StringUtils.PERSON_TEMPLATES);
				gameSession.setQuestion("Which economist is it?");

			} else if (selectedEntityType.equalsIgnoreCase("Company")) {
				rightEntities = new Entities(StringUtils.ECONOMY_COMPANY, StringUtils.COMPANY_PREDICATES,
						StringUtils.COMPANY_TEMPLATES);
				gameSession.setQuestion("Which company is it?");
			}
		}

		return rightEntities;
	}

	/**
	 * Starts the singleplayer game
	 */
	public void initGame() {
		if (!gameSession.isPlaying()) {
			Entities foundRightEntities = findRightEntities(gameSession.getSelectedTheme(),
					gameSession.getSelectedEntityType()); // find right entities (it depends from the selection of the
															// player in game-settings)
			String[] foundRightEntityArray = foundRightEntities.getEntityArray();
			String winningEntity = SPARQLUtils.chooseRandomlyEntity(foundRightEntityArray);
			String winningEntityURI = "<http://dbpedia.org/resource/" + winningEntity + ">";
			Map<String, String> entityImageMap = findImages(winningEntityURI);

			// in case an entity has no yago predicates it can't find similar entities. so
			// find a new winning entity as long as entityImageMap is not null anymore
			// otherwise, while playing, an error would occur
			while (entityImageMap.isEmpty()) {
				winningEntity = SPARQLUtils.chooseRandomlyEntity(foundRightEntityArray);
				winningEntityURI = "<http://dbpedia.org/resource/" + winningEntity + ">";
				entityImageMap = findImages(winningEntityURI);
			}

			List<String> generatedTips = generateTips(winningEntityURI, foundRightEntities.getPredicateArray(),
					foundRightEntities.getTemplateArray());

			gameSession.reset();

			gameSession.setPlaying(true);
			gameSession.setWinningEntity(winningEntity.replace("_", " "));
			gameSession.setTipsList(generatedTips);
			gameSession.setEntityImageMap(entityImageMap);
		}
	}

	/**
	 * Starts the next round
	 */
	public void nextRound() {
		if (gameSession.getRound() <= 3) {
			Entities foundRightEntities = findRightEntities(gameSession.getSelectedTheme(),
					gameSession.getSelectedEntityType()); // find right entities (it depends from the selection of the
															// player in game-settings)
			String[] foundRightEntityArray = foundRightEntities.getEntityArray();
			String winningEntity = SPARQLUtils.chooseRandomlyEntity(foundRightEntityArray);
			String winningEntityURI = "<http://dbpedia.org/resource/" + winningEntity + ">";
			Map<String, String> entityImageMap = findImages(winningEntityURI);

			// in case an entity has no yago predicates it can't find similar entities. so
			// find a new winning entity as long as entityImageMap is not null anymore
			// otherwise, while playing, an error would occur
			while (entityImageMap.isEmpty()) {
				winningEntity = SPARQLUtils.chooseRandomlyEntity(foundRightEntityArray);
				winningEntityURI = "<http://dbpedia.org/resource/" + winningEntity + ">";
				entityImageMap = findImages(winningEntityURI);
			}

			List<String> generatedTips = generateTips(winningEntityURI, foundRightEntities.getPredicateArray(),
					foundRightEntities.getTemplateArray());

			gameSession.setCountTip(1);
			gameSession.setRightSelection(false);
			gameSession.setSelected(false);
			gameSession.setWinningEntity(winningEntity.replace("_", " "));
			gameSession.setTipsList(generatedTips);
			gameSession.setEntityImageMap(entityImageMap);
		}
	}

	/**
	 * Evaluates the selected entity
	 * 
	 * @param selectedEntity
	 * @return evaluation as string
	 */
	public String selectEntity(String selectedEntity) {
		gameSession.increaseRound();

		if (gameSession.getRound() >= 3) {
			gameSession.setPlaying(false);
		}

		return gameSession.evaluateSelection(selectedEntity);
	}

	/**
	 * Increased counTip by 1
	 */
	public void increaseCountTip() {
		gameSession.increaseCountTip();
	}

	/**
	 * Just a test method which tests if every entity in an entity-array has enough tips.
	 */
	public void testEntities() {
		String[] entities = StringUtils.MUSIC_BAND;
		for (int i = 0; i < entities.length; i++) {
			List<String> generatedTips = generateTips("<http://dbpedia.org/resource/" + entities[i] + ">",
					StringUtils.BAND_PREDICATES, StringUtils.BAND_TEMPLATES);
			if (generatedTips.size() < 3) {
				System.out.println("Not enough tips: " + entities[i]);
				System.out.println("Generated tips: " + generatedTips.toString());
			}
		}

		System.out.println("Finished");
	}
}
