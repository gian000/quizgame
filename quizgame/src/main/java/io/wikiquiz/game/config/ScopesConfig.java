package io.wikiquiz.game.config;

import io.wikiquiz.game.model.GameMultiplayerSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.SessionScope;

import io.wikiquiz.game.model.GameSession;

@Configuration
public class ScopesConfig {
    @Bean
    @SessionScope
    public GameSession gameSession() {
        return new GameSession();
    }
    
    @Bean
    @SessionScope
    public GameMultiplayerSession gameMultiplayerSession() {
        return new GameMultiplayerSession();
    }
}
