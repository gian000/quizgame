package io.wikiquiz.game.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * GameMultiplayerSession class
 * This session will start when the multiplayer match started. It represents the multiplayer match itself.
 *
 */
public class GameMultiplayerSession {
	private Player player1;
	private Player player2;
    private int selectingPlayer;
    private int round;
    private int countTip;
    private boolean isPlaying;
    private boolean isRightSelection;
    private boolean selected;
    private String question;
    private String winningEntity;
    private List<String> tipsList;
    private Map<String, String> entityImageMap;

    private String selectedEntityType;
    private String selectedTheme;
    private String selectedMode;
    
    
    private static final int MAX_ROUNDS = 6;
    private static final int MAX_TIPS = 3;
    private static final int RIGHT_AFTER_FIRST = 300;
    private static final int RIGHT_AFTER_SECOND = 200;
    private static final int RIGHT_AFTER_THIRD = 100;
    private static final int WRONG_AFTER_FIRST = -200;
    private static final int WRONG_AFTER_SECOND = -125;
    private static final int WRONG_AFTER_THIRD = -50;

    /**
     * Initiates multiplayer match
     */
    public GameMultiplayerSession() {
    	this.player1 = new Player("Player1");
    	this.player2 = new Player("Player2");
        this.selectingPlayer = 1;
        this.round = 0;
        this.countTip = 1;
        this.isPlaying = false;
        this.isRightSelection = false;
        this.selected = false;
        this.winningEntity = "";
        this.tipsList = new ArrayList<String>();
        this.entityImageMap = new LinkedHashMap<String, String>();
    }

    /**
     * Depending on whether the selected entity is the correct one, the player receives or loses points (multiplayer).
     * @param selectedEntity - the selected entity while playing
     * @return Message as string which is later displayed after the selection of the entity
     */
    public String evaluateSelection(String selectedEntity) {
        selected = true;
        if(countTip == 1) {
            if(selectedEntity.equals(winningEntity)) {
                return rightAfterFirstTip();
            }else {
                return wrongAfterFirstTip();
            }
        }else if(countTip == 2) {
            if(selectedEntity.equals(winningEntity)) {
                return rightAfterSecondTip();
            }else {
                return wrongAfterSecondTip();
            }
        }else if(countTip == 3) {
            if(selectedEntity.equals(winningEntity)) {
                return rightAfterThirdTip();
            }
        }

        return wrongAfterThirdTip();
    }

    /**
     * Increases the round by 1.
     */
    public void increaseRound() {
        if(round < MAX_ROUNDS) {
            this.round++;
        }
    }

    /**
     * Increases the countTip by 1.
     */
    public void increaseCountTip() {
        if(countTip < MAX_TIPS) {
            this.countTip++;
        }
    }

    /**
     * Resets the multiplayer match
     */
    public void reset() {
        player1.setPoints(0);
        player2.setPoints(0);
        this.selectingPlayer = 1;
        this.round = 0;
        this.countTip = 1;
        this.isPlaying = false;
        this.isRightSelection = false;
        this.selected = false;
        this.winningEntity = "";
        this.tipsList = new ArrayList<String>();
        this.entityImageMap = new LinkedHashMap<String, String>();
    }

    public String rightAfterFirstTip() {
        isRightSelection = true;
        if(selectingPlayer == 1) {
        	player1.setPoints(player1.getPoints() + RIGHT_AFTER_FIRST);
        }else {
        	player2.setPoints(player2.getPoints() + RIGHT_AFTER_FIRST);
        }
        return "Very nice! \\^o^/ You selected the right one after the first tip. (" + winningEntity + ") + " + RIGHT_AFTER_FIRST;
    }

    public String rightAfterSecondTip() {
        isRightSelection = true;
        if(selectingPlayer == 1) {
        	player1.setPoints(player1.getPoints() + RIGHT_AFTER_SECOND);
        }else {
        	player2.setPoints(player2.getPoints() + RIGHT_AFTER_SECOND);
        }
        return "Nice! (*^▽^*) You selected the right one after second tip. (" + winningEntity + ") + " + RIGHT_AFTER_SECOND;
    }

    public String rightAfterThirdTip() {
        isRightSelection = true;
        if(selectingPlayer == 1) {
        	player1.setPoints(player1.getPoints() + RIGHT_AFTER_THIRD);
        }else {
        	player2.setPoints(player2.getPoints() + RIGHT_AFTER_THIRD);
        }
        return "Not bad! (oﾟvﾟ)ノ You selected the right one after the last tip. (" + winningEntity + ") + " + RIGHT_AFTER_THIRD;
    }

    public String wrongAfterFirstTip() {
        isRightSelection = false;
        if(selectingPlayer == 1) {
        	player1.setPoints(player1.getPoints() + WRONG_AFTER_FIRST);
        }else {
        	player2.setPoints(player2.getPoints() + WRONG_AFTER_FIRST);
        }
        return "Ouh! ＞︿＜ You selected the wrong one after the first tip. The right answer was " + winningEntity + ". " + WRONG_AFTER_FIRST;
    }

    public String wrongAfterSecondTip() {
        isRightSelection = false;
        if(selectingPlayer == 1) {
        	player1.setPoints(player1.getPoints() + WRONG_AFTER_SECOND);
        }else {
        	player2.setPoints(player2.getPoints() + WRONG_AFTER_SECOND);
        }
        return "Wrong one, but keep trying! ( ͡• ͜ʖ ͡• ) You selected the wrong one after the second tip. The right answer was " + winningEntity + ". " + WRONG_AFTER_SECOND;
    }

    public String wrongAfterThirdTip() {
        isRightSelection = false;
        if(selectingPlayer == 1) {
        	player1.setPoints(player1.getPoints() + WRONG_AFTER_THIRD);
        }else {
        	player2.setPoints(player2.getPoints() + WRONG_AFTER_THIRD);
        }
        return "At least you tried! ¯\\_(ツ)_/¯ You selected the wrong one after the last tip. The right answer was " + winningEntity + ". " + WRONG_AFTER_THIRD;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public int getCountTip() {
        return countTip;
    }

    public void setCountTip(int countTip) {
        this.countTip = countTip;
    }

    public String getWinningEntity() {
        return winningEntity;
    }

    public void setWinningEntity(String winningEntity) {
        this.winningEntity = winningEntity;
    }

    public List<String> getTipsList() {
        return tipsList;
    }

    public void setTipsList(List<String> tipsList) {
        this.tipsList = tipsList;
    }

    public Map<String, String> getEntityImageMap() {
        return entityImageMap;
    }

    public void setEntityImageMap(Map<String, String> entityImageMap) {
        this.entityImageMap = entityImageMap;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public boolean isRightSelection() {
        return isRightSelection;
    }

    public void setRightSelection(boolean isRightSelection) {
        this.isRightSelection = isRightSelection;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getSelectingPlayer() {
        return selectingPlayer;
    }

    public void setSelectingPlayer(int selectingPlayer) {
        this.selectingPlayer = selectingPlayer;
    }

    public String getSelectedEntityType() {
        return selectedEntityType;
    }

    public void setSelectedEntityType(String selectedEntityType) {
        this.selectedEntityType = selectedEntityType;
    }

    public String getSelectedTheme() {
        return selectedTheme;
    }

    public void setSelectedTheme(String selectedTheme) {
        this.selectedTheme = selectedTheme;
    }

    public String getSelectedMode() {
        return selectedMode;
    }

    public void setSelectedMode(String selectedMode) {
        this.selectedMode = selectedMode;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}
}
