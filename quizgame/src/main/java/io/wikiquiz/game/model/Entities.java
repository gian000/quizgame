package io.wikiquiz.game.model;

/**
 * 
 * Entities class
 *
 */
public class Entities {
	private String[] entityArray;
	private String[] predicateArray;
	private String[] templateArray;
	
	public Entities(String[] entityArray, String[] predicateArray, String[] templateArray) {
		this.entityArray = entityArray;
		this.predicateArray = predicateArray;
		this.templateArray = templateArray;
	}

	public String[] getEntityArray() {
		return entityArray;
	}

	public void setEntityArray(String[] entityArray) {
		this.entityArray = entityArray;
	}

	public String[] getPredicateArray() {
		return predicateArray;
	}

	public void setPredicateArray(String[] predicateArray) {
		this.predicateArray = predicateArray;
	}

	public String[] getTemplateArray() {
		return templateArray;
	}

	public void setTemplateArray(String[] templateArray) {
		this.templateArray = templateArray;
	}
}
