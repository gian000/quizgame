package io.wikiquiz.game.model;
/**
 * 
 * Player class
 * Is required to represent players in the singleplayer-mode and multiplayer-mode
 *
 */
public class Player {
	private String name;
	private int points;
	
	public Player() {
		this.points = 0;
	}
	
	public Player(String name) {
		this.name = name;
		this.points = 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}
}
