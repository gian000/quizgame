package io.wikiquiz.game.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/**
 * 
 * GameSession class
 * This session will start when the singleplayer match started. It represents the singleplayer match itself.
 *
 */
public class GameSession {
	private Player player;
	private int round;
	private int countTip;
	private boolean isPlaying;
	private boolean isRightSelection;
	private boolean selected;
	private String question;
	private String winningEntity;
	private List<String> tipsList;
	private Map<String, String> entityImageMap;
	
	private String selectedEntityType;
	private String selectedTheme;
	private String selectedMode;

	private static final int MAX_ROUNDS = 3;
	private static final int MAX_TIPS = 3;
	private static final int RIGHT_AFTER_FIRST = 300;
	private static final int RIGHT_AFTER_SECOND = 200;
	private static final int RIGHT_AFTER_THIRD = 100;
	private static final int WRONG_AFTER_FIRST = -200;
	private static final int WRONG_AFTER_SECOND = -125;
	private static final int WRONG_AFTER_THIRD = -50;

    /**
     * Initiates singleplayer match
     */
	public GameSession() {
		this.player = new Player("Player1");
		this.round = 0;
		this.countTip = 1;
		this.isPlaying = false;
		this.isRightSelection = false;
		this.selected = false;
		this.winningEntity = "";
		this.tipsList = new ArrayList<String>();
		this.entityImageMap = new LinkedHashMap<String, String>();
	}

    /**
     * Depending on whether the selected entity is the correct one, the player receives or loses points (singleplayer).
     * @param selectedEntity - the selected entity while playing
     * @return Message as string which is later displayed after the selection of the entity
     */
	public String evaluateSelection(String selectedEntity) {
		selected = true;
		if(countTip == 1) {
			if(selectedEntity.equals(winningEntity)) {
				return rightAfterFirstTip();
			}else {
				return wrongAfterFirstTip();
			}
		}else if(countTip == 2) {
			if(selectedEntity.equals(winningEntity)) {
				return rightAfterSecondTip();
			}else {
				return wrongAfterSecondTip();
			}
		}else if(countTip == 3) {
			if(selectedEntity.equals(winningEntity)) {
				return rightAfterThirdTip();
			}
		}
		
		return wrongAfterThirdTip();
	}
	
    /**
     * Increases the round by 1.
     */
	public void increaseRound() {
		if(round < MAX_ROUNDS) {
			this.round++;
		}	
	}

    /**
     * Increases the countTip by 1.
     */
	public void increaseCountTip() {
		if(countTip < MAX_TIPS) {
			this.countTip++;
		}	
	}
	
    /**
     * Resets the singleplayer match
     */
	public void reset() {
		this.player.setPoints(0);
		this.round = 0;
		this.countTip = 1;
		this.isPlaying = false;
		this.isRightSelection = false;
		this.selected = false;
		this.winningEntity = "";
		this.tipsList = new ArrayList<String>();
		this.entityImageMap = new LinkedHashMap<String, String>();
	}

	public String rightAfterFirstTip() {
		isRightSelection = true;
		player.setPoints(player.getPoints() + RIGHT_AFTER_FIRST);
		return "Very nice! \\^o^/ You selected the right one after the first tip. (" + winningEntity + ") + " + RIGHT_AFTER_FIRST;
	}

	public String rightAfterSecondTip() {
		isRightSelection = true;
		player.setPoints(player.getPoints() + RIGHT_AFTER_SECOND);
		return "Nice! (*^▽^*) You selected the right one after second tip. (" + winningEntity + ") + " + RIGHT_AFTER_SECOND;
	}

	public String rightAfterThirdTip() {
		isRightSelection = true;
		player.setPoints(player.getPoints() + RIGHT_AFTER_THIRD);
		return "Not bad! (oﾟvﾟ)ノ You selected the right one after the last tip. (" + winningEntity + ") + " + RIGHT_AFTER_THIRD;
	}

	public String wrongAfterFirstTip() {
		isRightSelection = false;
		player.setPoints(player.getPoints() + WRONG_AFTER_FIRST);
		return "Ouh! ＞︿＜ You selected the wrong one after the first tip. The right answer was " + winningEntity + ". " + WRONG_AFTER_FIRST;
	}

	public String wrongAfterSecondTip() {
		isRightSelection = false;
		player.setPoints(player.getPoints() + WRONG_AFTER_SECOND);
		return "Wrong one, but keep trying! ( ͡• ͜ʖ ͡• ) You selected the wrong one after the second tip. The right answer was " + winningEntity + ". " + WRONG_AFTER_SECOND;
	}

	public String wrongAfterThirdTip() {
		isRightSelection = false;
		player.setPoints(player.getPoints() + WRONG_AFTER_THIRD);
		return "At least you tried! ¯\\_(ツ)_/¯ You selected the wrong one after the last tip. The right answer was " + winningEntity + ". " + WRONG_AFTER_THIRD;
	}
	
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}
	
	public int getCountTip() {
		return countTip;
	}

	public void setCountTip(int countTip) {
		this.countTip = countTip;
	}

	public String getWinningEntity() {
		return winningEntity;
	}

	public void setWinningEntity(String winningEntity) {
		this.winningEntity = winningEntity;
	}

	public List<String> getTipsList() {
		return tipsList;
	}

	public void setTipsList(List<String> tipsList) {
		this.tipsList = tipsList;
	}

	public Map<String, String> getEntityImageMap() {
		return entityImageMap;
	}

	public void setEntityImageMap(Map<String, String> entityImageMap) {
		this.entityImageMap = entityImageMap;
	}

	public boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	public boolean isRightSelection() {
		return isRightSelection;
	}

	public void setRightSelection(boolean isRightSelection) {
		this.isRightSelection = isRightSelection;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getSelectedEntityType() {
		return selectedEntityType;
	}

	public void setSelectedEntityType(String selectedEntityType) {
		this.selectedEntityType = selectedEntityType;
	}

	public String getSelectedTheme() {
		return selectedTheme;
	}

	public void setSelectedTheme(String selectedTheme) {
		this.selectedTheme = selectedTheme;
	}

	public String getSelectedMode() {
		return selectedMode;
	}

	public void setSelectedMode(String selectedMode) {
		this.selectedMode = selectedMode;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
}
