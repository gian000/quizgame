package io.wikiquiz.game;


import java.util.List;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import io.wikiquiz.game.dao.JenaDao;
import io.wikiquiz.game.service.GameService;
import io.wikiquiz.game.util.SPARQLUtils;
import io.wikiquiz.game.util.StringUtils;


@SpringBootApplication
public class QuizgameApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuizgameApplication.class, args);

		
		/*
		GameService gameService = new GameService();
		String entityURI = "<http://dbpedia.org/resource/Netflix>";
		List<String> objectsInTemplates = gameService.generateTips(entityURI, StringUtils.COMPANY_PREDICATES, StringUtils.COMPANY_TEMPLATES);
		objectsInTemplates.forEach(v -> System.out.println(v));
		
		
		Map<String, String> entityWithImage = gameService.findImages(entityURI);
		entityWithImage.forEach((k, v) -> System.out.println(k + " " + v));
		*/
		
		/*
		GameService gameService2 = new GameService();
		String entityURI2 = "<http://dbpedia.org/resource/Thomas_Müller>";
		List<String> objectsInTemplates2 = gameService2.generateTips(entityURI2, StringUtils.PERSON_PREDICATES, StringUtils.PERSON_TEMPLATES);
		objectsInTemplates2.forEach(v -> System.out.println(v));
		
		Map<String, String> entityWithImage2 = gameService2.findImages(entityURI2);
		entityWithImage2.forEach((k, v) -> System.out.println(k + " " + v));
		*/
	}
}
