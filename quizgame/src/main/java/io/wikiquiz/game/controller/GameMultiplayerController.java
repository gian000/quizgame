package io.wikiquiz.game.controller;

import javax.annotation.Resource;

import io.wikiquiz.game.model.GameMultiplayerSession;
import io.wikiquiz.game.service.GameMultiplayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 
 * Controller class for the multiplayer mode
 *
 */
@Controller
public class GameMultiplayerController {
    @Autowired
    private GameMultiplayerService gameMultiplayerService;

    @Resource(name = "gameMultiplayerSession")
    private GameMultiplayerSession gameMultiplayerSession;

    @GetMapping("/gameMultiplayer")
    public String playing(Model model) {
    	// if entity type has not been selected before then we know that the player was not playing
    	//and therefore he gets redirected to game-settings
        if(gameMultiplayerSession.getSelectedEntityType() == null) {
            return "redirect:/game-settings";
        }
        
        // otherwise initiate the multiplayer match
        gameMultiplayerService.initGame();
        model.addAttribute("gameMultiplayerSession", gameMultiplayerSession);
        return "game-multiplayer";
    }

    @PostMapping("/gameMultiplayer")
    public String revealTipForm(Model model) {
    	gameMultiplayerService.increaseCountTip();
        model.addAttribute("gameMultiplayerSession", gameMultiplayerSession);
        System.out.println("selecting Player: " + gameMultiplayerSession.getSelectingPlayer());
        System.out.println("round = " + gameMultiplayerSession.getRound());
        System.out.println("countTip = " + gameMultiplayerSession.getCountTip());
        System.out.println("post /gameMultiplayer: Current points Player1: " + gameMultiplayerSession.getPlayer1().getPoints());
        System.out.println("post /gameMultiplayer: Current points Player2: " + gameMultiplayerSession.getPlayer2().getPoints());
        return "game-multiplayer";
    }

    @PostMapping("/gameMultiplayer/selected")
    public String selectEntityForm(Model model, @ModelAttribute("selectedEntity") String selectedEntity) {
        System.out.println("chosen entity from page: " + selectedEntity);
        String evaluationMessage = gameMultiplayerService.selectEntity(selectedEntity);

        model.addAttribute("evaluationMessage", evaluationMessage);
        model.addAttribute("gameMultiplayerSession", gameMultiplayerSession);
        System.out.println("/game/choosed: Current points Player1: " + gameMultiplayerSession.getPlayer1().getPoints());
        System.out.println("/game/choosed: Current points Player2: " + gameMultiplayerSession.getPlayer2().getPoints());
        System.out.println("current round: " + gameMultiplayerSession.getRound());
        System.out.println("selecting Player: " + gameMultiplayerSession.getSelectingPlayer());
        return "game-multiplayer";
    }

    @PostMapping("/gameMultiplayer/nextRound")
    public String nextRoundForm(Model model) {
    	gameMultiplayerService.nextRound();
    	
        System.out.println("get /gameMultiplayer: Current points Player1: " + gameMultiplayerSession.getPlayer1().getPoints());
        System.out.println("get /gameMultiplayer: Current points Player2: " + gameMultiplayerSession.getPlayer2().getPoints());
        model.addAttribute("gameMultiplayerSession", gameMultiplayerSession);
        return "game-multiplayer";
    }
}
