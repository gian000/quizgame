package io.wikiquiz.game.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import io.wikiquiz.game.model.GameSession;
import io.wikiquiz.game.service.GameService;

@Controller
public class GameController {
	@Autowired
	private GameService gameService;
	
	@Resource(name = "gameSession")
	private GameSession gameSession;
	
	@GetMapping("/game")
	public String playing(Model model) {
		if(gameSession.getSelectedEntityType() == null) {
			return "redirect:/game-settings";
		}
		gameService.initGame();
		model.addAttribute("gameSession", gameSession);
		return "game";
	}
	
	@PostMapping("/game")
	public String revealTipForm(Model model) {
		gameService.increaseCountTip();
		model.addAttribute("gameSession", gameSession);
		System.out.println("round = " + gameSession.getRound());
		System.out.println("countTip = " + gameSession.getCountTip());
		System.out.println("post /game: Current points: " + gameSession.getPlayer().getPoints());
		return "game";
	}
	
	@PostMapping("/game/selected")
	public String selectEntityForm(Model model, @ModelAttribute("selectedEntity") String selectedEntity) {
		System.out.println("chosen entity from page: " + selectedEntity);
		String evaluationMessage = gameService.selectEntity(selectedEntity);
		
	    model.addAttribute("evaluationMessage", evaluationMessage);
	    model.addAttribute("gameSession", gameSession);
	    System.out.println("/game/choosed: Current points: " + gameSession.getPlayer().getPoints());
	    System.out.println("current round: " + gameSession.getRound());
		return "game";
	}
	
	@PostMapping("/game/nextRound")
	public String nextRoundForm(Model model) {
		gameService.nextRound();
		
		System.out.println("get /game: Current points: " + gameSession.getPlayer().getPoints());
		model.addAttribute("gameSession", gameSession);
		return "game";
	}
}
