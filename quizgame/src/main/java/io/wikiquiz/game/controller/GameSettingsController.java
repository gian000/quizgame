package io.wikiquiz.game.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import io.wikiquiz.game.model.GameMultiplayerSession;
import io.wikiquiz.game.model.GameSession;
import io.wikiquiz.game.service.GameSettingsService;

@Controller
public class GameSettingsController {
	@Autowired
	private GameSettingsService gameSettingsService;
	
	@Resource(name = "gameSession")
	private GameSession gameSession;
	
	@Resource(name = "gameMultiplayerSession")
	private GameMultiplayerSession gameMultiplayerSession;
	
	@GetMapping("/game-settings")
	public String settings(Model model) {
		model.addAttribute("entityTypes", gameSettingsService.findEntityTypes());
		model.addAttribute("themes", gameSettingsService.findThemes());

		return "game-settings";
	}
	
	@PostMapping("/game-settings")
	public String applySettings(Model model,
			@ModelAttribute("selectedEntityType") String selectedEntityType,
			@ModelAttribute("selectedTheme") String selectedTheme,
			@ModelAttribute("mode") String selectedMode,
			@ModelAttribute("player1Name") String player1Name) {
		
		gameSettingsService.resetSettings();
		gameSettingsService.applySettings(selectedEntityType, selectedTheme, selectedMode, player1Name);
		return "redirect:/game";
	}
	
	@PostMapping("/game-settings/multiplayer")
	public String applyMultiplayerSettings(Model model,
			@ModelAttribute("selectedEntityType") String selectedEntityType,
			@ModelAttribute("selectedTheme") String selectedTheme,
			@ModelAttribute("mode") String selectedMode,
			@ModelAttribute("player1Name") String player1Name,
			@ModelAttribute("player2Name") String player2Name) {
		
		gameSettingsService.resetMultiplayerSettings();
		gameSettingsService.applyMultiplayerSettings(selectedEntityType, selectedTheme, selectedMode, player1Name, player2Name);
		return "redirect:/gameMultiplayer";
	}
}
